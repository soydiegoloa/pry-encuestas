<!-- index.php -->
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>CUESTIONARIO DE SATISFACCIÓN DEL ESTUDIANTE</title>
    <link rel="stylesheet" href="style.css">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
</head>
<body>
    <div class="header">
        <img src="logo.png" alt="Logo" class="logo">
    </div>
    <h1>CUESTIONARIO DE SATISFACCIÓN DEL ESTUDIANTE</h1>
    <form action="submit.php" method="post">
        <label for="student_id">Código de estudiante:</label>
        <input type="number" id="student_id" name="student_id" required><br>
        
        <label for="satisfaction_score">Puntuación de satisfacción (1-10):</label>
        <input type="number" id="satisfaction_score" name="satisfaction_score" min="1" max="10" required><br>
        
        <label for="feedback">Feedback (Información adicional del estudiante):</label>
        <textarea id="feedback" name="feedback"></textarea><br>
        
        <button type="submit">ENVIAR</button>
    </form>

    <a href="dashabord.php" class="btn btn-primary">Ver Cuestionarios Registrados</a>
</body>
</html>
