<?php
require 'db.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $student_id = $_POST['student_id'];
    $satisfaction_score = $_POST['satisfaction_score'];
    $feedback = $_POST['feedback'];

    $stmt = $pdo->prepare("INSERT INTO responses (student_id, satisfaction_score, feedback) VALUES (?, ?, ?)");
    $stmt->execute([$student_id, $satisfaction_score, $feedback]);

    echo '
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>GRACIAS</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div class="header">
            <img src="logo.png" alt="Logo" class="logo">
        </div>
        <h1>Gracias por tu comentario!</h1>
        <button onclick="goBack()">VOLVER</button>
        <script>
            function goBack() {
                window.location.href = "index.php";
            }
        </script>
    </body>
    </html>';
}
?>