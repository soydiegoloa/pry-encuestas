<!-- dashboard.php -->
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>CUESTIONARIOS REGISTRADOS</title>
    <link rel="stylesheet" href="style.css">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
</head>
<body>
    <div class="header">
        <img src="logo.png" alt="Logo" class="logo">
    </div>
    <h1>CUESTIONARIOS REGISTRADOS</h1>
    <canvas id="summaryChart" width="800" height="400"></canvas> <!-- Aumentado el tamaño -->

    <?php
    require 'db.php';
    
    $stmt = $pdo->query("SELECT satisfaction_score, COUNT(*) as count FROM responses GROUP BY satisfaction_score");
    $scores = [];
    $counts = [];

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $scores[] = $row['satisfaction_score'];
        $counts[] = $row['count'];
    }
    ?>

    <script>
        var ctx = document.getElementById('summaryChart').getContext('2d');
        var summaryChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: <?php echo json_encode($scores); ?>,
                datasets: [{
                    label: 'Número de encuestas registradas',
                    data: <?php echo json_encode($counts); ?>,
                    backgroundColor: 'rgba(75, 192, 192, 0.2)',
                    borderColor: 'rgba(75, 192, 192, 1)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    </script>
    <button onclick="window.location.href='index.php'">Volver al Cuestionario</button>
</body>
</html>
